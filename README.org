* Repozitorijum za projekat iz kursa Informacioni sistemi, MATF 2017.

** Inicijalno zaduženje
*** Petar Misic : koordinisati modeliranje baze podataka
*** Nikola Katic: koordinisati podelom posla, identifikacijom zadataka unošenjem istih u sistem za kontrolu verzija i koordinisati osmišljavanjem korisničkog interfejsa
*** Gorana Vucic: koordinisati pravljenjem predloga arhitekture, koordinisati osmišljavanjem inicijalne strukture repozitorijuma
*** Irena Blagojevic: koordinisati pravljenjem prototipa
