periodicna -> redovna


1. Logovanje na sistem (izlazenje se postize vadjenjem kartice, i nema potrebe za dodatnim slucejem "izlogovanja")

Snabdevanje magacina brigade

2. B4 salje zahtev za popunu magacina 
3. B4 prima odobrenje i obavestava rukovaoca magacina
4. Popuna magacina brigade na hitan zahtev rukovaoca magacina
5. B4 prihvata zahtev za hitnu popunu magacina (ovaj slucaj povlaci korake 2. pa 3. pa 6.)
6. Fizicka popuna magacina (treba da se prosiri alternativni slucaj nepoklapanja ali prihvatanja robe, tako da predstavlja podtok citav)

----------------------------------------------------------------------

Snabdevanje magacina bataljona

7. Periodicna popuna magacina bataljona (u okviru ovoga je ukljucena i fizicka popuna)


7.1. B4 salje naredjenje log. bat. za popunu tog i tog bataljona (odredjenim mts) kao, ali salje i vreme DO KADA popuna treba da se obavi (istovari)


7.2. log. bataljon salje obavestenje ka B4 u koje vreme bataljon moze ocekivati popunu 
7.2.1. ili salje obavestenje zahtev za produzenje roka (alt. tok ili podtok)


7.3. B4 tu informaciju prosledjuje bataljonu koji se popunjava (i S4 i rukovaocu magacina bataljona koji treba da primi robu)


7.4. log.bat salje NAREDJENJE rukovaocu magacina brigade koja sredstava, kolicinu i vreme kada treba da ih spremi za transport


gg salje i naredjenje SVOJOJ ceti za transport o broju potrebnih vozila i ljudstva (u naredjenju se MORA navesti ko je odredjen za staresinu transporta) i navodi vreme kada ce stvari uzeti iz magacina brigade

 7.6. trenutak preuzimanja sredstava od strane staresine transporta iz magacina brigade (rukovaca magacina)
 7.6. S4 biva obavesten kada se izvrsi popuna magacina njegovog bataljona 
 ponavlja se postupak kao kod popune magacine brigade, kao i moguci alternativni tok

8. Popuna magacina bataljona na hitan zahtev (S4 salje zahtev za popunu)
   ako zahtev prodje, onda se dalje nastavlja kao u slucuju periodicne popune

9. Preraspodela sredstava izmedju magacina bataljona od strane S4 - to moze biti inicirano od strane samog S4, ali takodje i cetnog staresine

----------------------------------------------------------------------

Snabdevanje magacina cete 

11.Periodicna popuna magacina ceta
 S4 naredjuje rukovaocu magacina bataljona da izda odredjenu kolicinu mts odredjenim cetama 
 S4 naredjuje cetnim staresinama da ta mts u odredjeno vreme preuzmu iz magacina bataljona
 cetne staresine svojim transportom (svojim ljudstvom) preuzimaju sredstva iz magacina bataljona. prilikom preuzimanja mts, sredstva se prebrojavaju i proveravaju sto biva verifikovano od obe strane (na isti nacin kao i ranije samo sto ovde ne postoji alternativni tok da kolicine nema, jer cim je S4 odobrio, to znaci da sredstava ima)
 u trenutku kada je razmena verifikovana od obe strane, magacin cete je popunjen za taj mts automatski (iako realno treba da se saceka da fizicki udje u magacin. to se tako realizuje jer je isti covek odgovoran, tj. duzi ta sredstava - *cetni staresina*)

12.Popuna magacina ceta na hitan zahtev (cetni staresina salje zahtev za popunu)

14.Preraspodela sredstava izmedju magacina ceta

15. *opciono:* Automatsko generisanje predloga preraspodele sredstava izmedju ceta (ovo bi mogao samo da bude predlog koji nece biti u upotrebi, vec ce samo biti aktivan kako bi ucio nad podacima)

+16.Vojnik uzima sredstva iz magacina+
+17.Vojnik vraca sredstva u magacin+

----------------------------------------------------------------------





