datum 31.10.2017 - odgovori na nejasnoce

* ... -> izvidjacki vod
* b i s su samo nivo
**  1-personalno
**  2-obavestajno-izvidjacki poslovi
**  3-planiranje i obuka
**  4-snabdevanje i logistika
**  5-obuka (na nivou bataljona ne postoji nego je spojena u s3, a na nivou brigade je b3 planiranje, a b5 obuka)
**  6-veza i informaticka podrska
***  ova 6-ica bi mogla biti korisna u smislu da bi oni mogli da budu admini


* 4.
** staresina ima uvid samo u svoj magacin
** svaki staresina zasebno zahtev (ne postoji softver)
*** iskuca ga, istampa ga, posalje ga fizicki (a posalje ga i elektronski, unutrasnjom mrezom)
** slucaj upotrebe:
   prva ceta se salje na zadatak odbrane od poplava (ili sumskog pozara) 
   u magacinu postoji hrana za 2 dana, a ceta ide na zadatak od nedelju dana
   u tom slucaju ce cetne staresine drugih ceta dati hranu ceti koja ide na zadatak
   
** b.
   ili se ostecena oprema menja ispravnom, ili se oprema izdaje prema zadatku (obuci)
   ima knjiga izdavanja i vracanja opreme (u nju se upisuje da je izdata ....)
   
   cetni staresina je tu da licno izdaje stvari (kao i rukovalac magacina brigade)

   svaki vojnik ima svoj LZ karton (karton licnog zaduzenja) u kojem pise trenutno stanje (koliko je zaduzio cega tacno), i kada odlazi u drugu jedinicu ili prekomandu, onda cetni staresina uzme taj njegov karton i sve isproverava da li je i dalje ispravno i na broju
   ideja je da bi LZ takodje mogao da se evidentira elektronski - jedino sto bi bilo neophodno to je da postoji neki vid licne potvrde elektronskog potpisa.... sluzbena legitimacija vojnika je cipovana (+ ima svoj pin), pa bi ubacivanje kartice (kompatibilno sa standardnim citacem kartica) i ukucavanje pina potvrdjivalo identitet

* 5.
** ne mora svaki bataljon da ima intedantski servis vec svaka kasarna (objekat u kojem se nalazi odredjena jedinica ranga bataljona ili veceg ranga), aerodrom, luka... trebalo bi da ima 1
** b. 
   bave se usluznom delatnoscu 
   moze da im pristupa bilo ko
   nemaju stvari kao sto su municija i naoruzanje, imaju samo uniforme, cizme, ranceve...
** c.
   najcesce se odmah postave ogranicenja - sta se iznosi, a sta se ne iznosi 
   
   3.primer poplava, dato je naredjenje da se municija uzima u veoma maloj kolicini... a iz bataljonskog nista sem gumenih camaca
   u slucaju 1. 2. i 4. se iz cetnog i iz bataljonskog nacelno uzima sve (pa i iz brigadnog)

** e. nije uvek duzan da pita B4

** intedantskim servisom rukovidi (komanduje) komandir int. servisa (po cinu stariji vodnik prve klase ili zastavnik)
   

