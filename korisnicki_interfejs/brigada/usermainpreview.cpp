#include "usermainpreview.h"
#include "userinfoscreen.h"
#include "warehousepreview.h"
#include "neworder.h"

#include "ui_usermainpreview.h"

#include <QPixmap>
#include <QIcon>
#include <QFile>
#include <QTextStream>
#include <iostream>
#include <QList>

UserMainPreview::UserMainPreview(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UserMainPreview)
{
    ui->setupUi(this);
    this->setWindowTitle("Informacioni sistem Vojske Srbije");

    unreadOrders = 1;
    unreadPleas = 0;
    unreadAnnoun = 3;
    unreadClaims = 0;
    totalUnread = unreadOrders + unreadPleas + unreadAnnoun + unreadClaims;

    ordersLoaded = false;

    QPixmap pixmap(":user.png");
    QSize buttonSize(ui->userButton->width(),
                     ui->userButton->height());
    QIcon ButtonIcon(pixmap);
    ui->userButton->setIcon(ButtonIcon);
    ui->userButton->setIconSize(buttonSize);
    connect(ui->userButton, &QPushButton::clicked,
            this, &UserMainPreview::viewUserInfo);

    userPreview = new UserInfoScreen(this);
    unitChooser = new UnitChooser(this);
    warehousePreview = new WarehousePreview(this);
    newOrder = new NewOrder(this);

    connect(ui->troopBtn, &QPushButton::clicked,
            this, &UserMainPreview::chooseTroop);

    connect(ui->ordersBtn, &QPushButton::clicked,
            this, &UserMainPreview::viewOrders);

    // "removing" i.e. disabling close buttons on first two tabs
    ui->mainTab->tabBar()->tabButton(0, QTabBar::RightSide)->resize(0, 0);
    ui->mainTab->tabBar()->tabButton(1, QTabBar::RightSide)->resize(0, 0);

    ui->mainTab->removeTab(2);
    ui->mainTab->removeTab(2);
    ui->sentOrdersTree->header()->resizeSections(QHeaderView::ResizeToContents);

    connect(ui->mainTab, &QTabWidget::tabCloseRequested,
            this, &UserMainPreview::closeTab);

    // double click to open a certain order
    connect(ui->ordersTree, &QTreeWidget::doubleClicked,
            this, &UserMainPreview::showOrder);

    connect(ui->newOrderBtn, &QPushButton::clicked,
            this, &UserMainPreview::createNewOrder);
}

UserMainPreview::~UserMainPreview()
{
    delete ui;
}

void UserMainPreview::viewUserInfo()
{
    userPreview->show();
}

void UserMainPreview::chooseTroop()
{
    /* Wait until user makes his choice in the chooser dialog*/
    if( unitChooser->exec() == QDialog::Accepted ) {
        //this->hide();
        warehousePreview->show();
    }
}

void UserMainPreview::viewOrders()
{
    loadReceivedOrders();
    ordersLoaded = true;
    ui->ordersTree->header()->resizeSections(QHeaderView::ResizeToContents);
    ui->mainTab->addTab(ui->ordersTab, "Naređenja");
    ui->mainTab->setCurrentIndex(2);
}

void UserMainPreview::createNewOrder()
{
    newOrder->show();
}

void UserMainPreview::closeTab(int index)
{
    ui->mainTab->removeTab(index);
    ui->mainTab->setCurrentIndex(0);
}

void UserMainPreview::showOrder()
{
    int colnum = ui->ordersTree->selectedItems().at(0)->columnCount();
    // if selected order wasn't read,  mark it as read and update counters
    if (ui->ordersTree->selectedItems().at(0)->font(0).bold()) {
        unreadOrders--;
        totalUnread--;
        // mark order as read
        for (int i = 0; i < colnum; ++i) {
            ui->ordersTree->selectedItems().at(0)->setFont(i, QFont("sans serif"));
            updateReadOrder(ui->ordersTree->currentIndex().row());
        }
        // update number of unread orders in orders tab
        if (unreadOrders == 0) {
            ui->ordersBtn->setText("Naređenja (0)");
            ui->ordersBtn->setFont(QFont("sans serif", 9));
            ui->ordersBtn->setStyleSheet("");
        }
        else {
            QString str = "Naređenja (" + QString::number(unreadOrders) + ")";
            ui->ordersBtn->setText(str);
        }

        // update total number of unread messages
        if (totalUnread == 0)
            ui->mainTab->setTabText(0, "Poruke (0)");
        else
            ui->mainTab->setTabText(0, "Poruke (" + QString::number(totalUnread) + ")");
    }
    QString subject = ui->ordersTree->selectedItems().at(0)->text(3);
    QString sender = ui->ordersTree->selectedItems().at(0)->text(1);
    QString date = ui->ordersTree->selectedItems().at(0)->text(2);
    orderPreview = new OrderPreview(this, ui->ordersTree->currentIndex().row());
    orderPreview->show();
}

void UserMainPreview::showSentOrders()
{
    ui->mainTab->addTab(ui->sentOrdersTab, "Poslata naređenja");
    ui->mainTab->setCurrentIndex(3);
}

void UserMainPreview::loadReceivedOrders()
{
    QString filename = ":/received_orders.txt";
    QFile inputFile(filename);
    // If orders are already loaded, do nothing
    if (ordersLoaded)
        return;
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        int row = 0;
        while (!in.atEnd())
        {
            int col = 0, unread = 0, checked = 0;
            QString line = in.readLine();
            if (line == "unread") {
                unread = 1;
            }
            QList<QString> columns;
            // Read columns for current row
            while ((line = in.readLine()) != "#!") {
                if (col == 0) {
                    if (line.compare("c") == 0)
                        checked = 1;
                    columns.append("");
                }
                else if (col > 3) {
                    continue;
                }
                else {
                    //item->setData(row, Qt::DisplayRole, );
                    columns.append(line);
                }
                col++;
            }
            QTreeWidgetItem *item = new QTreeWidgetItem(columns);
            // Set font to bold for unread orders
            for (int i = 0; i < item->columnCount(); ++i) {
                if (unread)
                    item->setFont(i, QFont("sans serif", -1, QFont::Bold));
            }
            // Set check state for each order
            if (checked)
                item->setCheckState(0, Qt::Checked);
            else
                item->setCheckState(0, Qt::Unchecked);
            ui->ordersTree->addTopLevelItem(item);
            row++;
        }
    }
    inputFile.close();
}

void UserMainPreview::updateReadOrder(int orderno)
{
    QString filename = ":/received_orders.txt";
    QFile file(filename);
    if (file.open(QIODevice::ReadWrite | QIODevice::Append))
    {
        QTextStream in(&file);
        int i = 0;
        QString line;
        while (i != orderno) {
            line = in.readLine();
            if (line.compare("#!") == 0)
                i++;
        }
        // Mark order as read in the file
        file.write("read\n");
    }
    file.close();
}
