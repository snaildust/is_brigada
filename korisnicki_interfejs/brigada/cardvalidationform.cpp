#include "cardvalidationform.h"
#include "ui_cardvalidationform.h"

#include "mainwindow.h"

#include <QShortcut>
#include <QRegExpValidator>
#include <QRegExp>
#include <QMessageBox>

CardValidationForm::CardValidationForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CardValidationForm)
{
    ui->setupUi(this);
    this->setWindowTitle("Autentikacija korisnika");
    ui->passphraseLine->setFocus();

    QRegExpValidator* valid_input = new QRegExpValidator(QRegExp("\\w{4}"));
    ui->passphraseLine->setValidator(valid_input);

    failedAttemptsCnt = 0;
    ui->authenticationFailed->hide();
    ui->authenticationFailed->setShortcut(QKeySequence(Qt::Key_Meta + Qt::Key_1));
    // Workaround to simulate authentication failure
    QShortcut *forceAuthFail = new QShortcut(QKeySequence("Ctrl+F"), this);
    QObject::connect(forceAuthFail, &QShortcut::activated,
                     this, &CardValidationForm::authenticationAttemptFailed);

    ui->msgOnFail->setStyleSheet("QLabel { color : red; }");

    connect(ui->buttonBox, &QDialogButtonBox::accepted,
            this, &CardValidationForm::authenticateAttempt);
}

CardValidationForm::~CardValidationForm()
{
    delete ui;
}

/**
 * @brief CardValidationForm::startValidationProcedure
 * Authenticate by comparing with correct key from database
 */
void CardValidationForm::authenticateAttempt()
{
    // Until app is connected with a database, we will
    // assume that every 4-character key IS correct
    if (ui->passphraseLine->text().length() != 4)
        authenticationAttemptFailed();
    else
        authenticationAttemptSucceded();
}

void CardValidationForm::authenticationAttemptFailed()
{
    ui->authenticationMsg->setText("Pogrešan PIN! Molimo Vas pokušajte ponovo:");

    if (ui->passphraseLine->isEnabled())
        failedAttemptsCnt++;

    QString msg = "Ovo je " + QString::number(failedAttemptsCnt) + ". neuspešan";

    ui->msgOnFail->setText(msg + " pokušaj!");
    ui->msgOnFail->show();

    if (failedAttemptsCnt > 2)
        blockCurrentCard();
    show();
}

void CardValidationForm::authenticationAttemptSucceded()
{
    QMessageBox success(QMessageBox::Information, "Uspešno prijavljivanje",
                        "Autentikacija je uspešno izvršena!", QMessageBox::NoButton, this);
    this->hide();
    underlingsView.show();
    success.exec();
}

void CardValidationForm::blockCurrentCard()
{
    QString msg = "Vaša kartica je blokirana na nivou celog sistema."
                  " Molimo Vas, javite se administraciji.";
    ui->authenticationMsg->setText(msg);
    ui->authenticationMsg->setStyleSheet("QLabel { color : red; }");

    ui->passphraseLine->setEnabled(false);
}
