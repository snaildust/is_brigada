#include "warehousepreview.h"
#include "ui_warehousepreview.h"

#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QStringList>
#include <QString>
#include <QTableWidget>
#include <QDebug>
#include <QMessageBox>

WarehousePreview::WarehousePreview(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WarehousePreview)
{
    ui->setupUi(this);
    ui->warehouseTable->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);

    connect(ui->formListBtn, &QPushButton::clicked,
            this, &WarehousePreview::formList);

    connect(ui->malfuncBtn, &QPushButton::clicked,
            this, &WarehousePreview::logMalfunction);

    QString filename = ":/warehouse.txt";
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly))
    {
        QTextStream in(&file);
        int row = 0;
        QString line;
        while (!in.atEnd()) {
            line = in.readLine();

            QStringList itemcolumns = line.split("#");
            if (itemcolumns.size() < 8) {
                QString msg = "Ulazni fajl prototipa nije pravilno formatiran!"
                              "\nSvaka linija treba da bude sledeceg oblika:";
                msg += "\n\t <naziv_sredstva>##<merna_jedinica>#<na_stanju>";
                msg += "#<u_upotrebi>#<neispravno>#<za_rashod>#<minimum>";
                msg += "\n\nMolimo vas ponovo pokrenite program nakon izmene!";
                QMessageBox warn(QMessageBox::Critical, "Greska prototipa",
                                 msg, QMessageBox::NoButton, this);
                warn.exec();
                break;
            }

            ui->warehouseTable->insertRow(row);

            QTableWidgetItem *item;
            for (int i = 0; i < itemcolumns.length(); ++i) {
                ui->warehouseTable->setItem(
                            row, i, new QTableWidgetItem(itemcolumns.at(i)));
                item = ui->warehouseTable->item(row, i);
                item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            }
            ui->warehouseTable->setItem(row, 8, new QTableWidgetItem(""));

            // Add checkbox
            for (int i = 0; i < ui->warehouseTable->rowCount(); ++i)
                ui->warehouseTable->item(i, 1)->setCheckState(Qt::Unchecked);

            int valid_num = itemcolumns[3].toInt();
            valid_num -= itemcolumns[5].toInt() + itemcolumns[6].toInt();

            // check if:  (in_stock - for_repair - invalid < minimum)
            if ( valid_num < itemcolumns[7].toInt()) {
                for (int i = 0; i < ui->warehouseTable->columnCount()-1; ++i)
                    ui->warehouseTable->item(row, i)->setBackground(Qt::red);
                QString suggest = QString::number(itemcolumns[7].toInt()-valid_num);
                ui->warehouseTable->item(row, 8)->setText(suggest);
            }
            row++;
        }
    }
    file.close();

    ui->warehouseTable->resizeColumnsToContents();
}

WarehousePreview::~WarehousePreview()
{
    delete ui;
}

void WarehousePreview::formList()
{
    QString filename = "./attach-" +
            QDateTime::currentDateTime().toString("dd_MM_yyyy_hhmmss") + ".txt";
    QFile file(filename);
    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);

        int len = 0;
        for (int i = 0; i < ui->warehouseTable->rowCount(); ++i) {
            if (ui->warehouseTable->item(i, 1)->checkState() == Qt::Checked) {
                stream << ++len << ". ";
                stream << ui->warehouseTable->item(i, 0)->text();
                stream << " - " << ui->warehouseTable->item(i, 8)->text() << "\n";
            }
        }
    }


    file.close();

    // open new window with qtablewidget with selected items
    // + empty fileds for the amount
}

void WarehousePreview::logMalfunction()
{

}
