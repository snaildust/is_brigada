#include "orderpreview.h"
#include "ui_orderpreview.h"

#include <QFile>
#include <QTextStream>

OrderPreview::OrderPreview(QWidget *parent, int orderno) :
    QDialog(parent),
    ui(new Ui::OrderPreview)
{
    ui->setupUi(this);

    connect(ui->exitBtn, &QPushButton::clicked,
            this, &OrderPreview::closeOrder);

    connect(ui->forwardBtn, &QPushButton::clicked,
            this, &OrderPreview::forwardOrder);

    QPixmap pixmap(":paperclip.svg");
    ui->attachLbl->setPixmap(pixmap.scaled(15, 15));
    QString subject, sender, date;
    QString filename = ":/received_orders.txt";
    QFile inputFile(filename);
    QString text;
    if (inputFile.open(QIODevice::ReadOnly)) {
        QTextStream in(&inputFile);
        int row = 0;
        while (!in.atEnd()) {
            QString line;
            while (row != orderno) {
                line = in.readLine();
                if (line.compare("#!") == 0)
                    row++;
            }
            line = in.readLine(); // read/unread
            line = in.readLine();
            sender = in.readLine();
            date = in.readLine();
            subject = in.readLine();
            if ((line = in.readLine()).compare("#-") == 0)
                text = in.readLine();
            else
                text = "";
            break;
        }
    }
    this->setWindowTitle(subject);
    ui->detailsLbl->setText(sender + ": " + subject + " [" + date + "]");
    ui->orderText->setText(text);
}

OrderPreview::~OrderPreview()
{
    delete ui;
}

void OrderPreview::closeOrder()
{
    this->close();
}

void OrderPreview::forwardOrder()
{

}
