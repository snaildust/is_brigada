#ifndef OrderPreview_H
#define OrderPreview_H

#include <QDialog>

namespace Ui {
class OrderPreview;
}

class OrderPreview : public QDialog
{
    Q_OBJECT

public:
    explicit OrderPreview(QWidget *parent = 0,
                          int orderno = 0);
    ~OrderPreview();

public slots:
    void closeOrder();
    void forwardOrder();

private:
    Ui::OrderPreview *ui;
};

#endif // OrderPreview_H
