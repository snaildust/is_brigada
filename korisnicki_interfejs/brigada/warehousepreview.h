#ifndef WAREHOUSEPREVIEW_H
#define WAREHOUSEPREVIEW_H

#include <QDialog>

namespace Ui {
class WarehousePreview;
}

class WarehousePreview : public QDialog
{
    Q_OBJECT

public:
    explicit WarehousePreview(QWidget *parent = 0);
    ~WarehousePreview();

public slots:
    void formList();
    void logMalfunction();

private:
    Ui::WarehousePreview *ui;
};

#endif // WAREHOUSEPREVIEW_H
