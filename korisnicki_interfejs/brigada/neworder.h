#ifndef NEWORDER_H
#define NEWORDER_H

#include <QDialog>

namespace Ui {
class neworder;
}

class NewOrder : public QDialog
{
    Q_OBJECT

public:
    explicit NewOrder(QWidget *parent = 0);
    ~NewOrder();

public slots:
    void closeOrder();
    void forwardOrder();

private:
    Ui::neworder *ui;
};

#endif // NEWORDER_H
