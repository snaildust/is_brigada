#ifndef UNITCHOOSER_H
#define UNITCHOOSER_H

#include <QDialog>

namespace Ui {
class UnitChooser;
}

class UnitChooser : public QDialog
{
    Q_OBJECT

public:
    explicit UnitChooser(QWidget *parent = 0);
    ~UnitChooser();

private:
    Ui::UnitChooser *ui;
};

#endif // UNITCHOOSER_H
