#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QShortcut>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Infomacioni sistem Vojske Srbije");

    // Hide this button. To procced, use shortcut instead
    ui->cardInserted->hide();
    QShortcut *forceCardInsert = new QShortcut(QKeySequence("Ctrl+I"), this);
    QObject::connect(forceCardInsert, &QShortcut::activated,
                     this, &MainWindow::cardInserted);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::cardInserted()
{
    this->hide();
    cardValidation.show();
}
