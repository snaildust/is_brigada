#include "unitchooser.h"
#include "ui_unitchooser.h"

#include <QLineEdit>

UnitChooser::UnitChooser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UnitChooser)
{
    ui->setupUi(this);
}

UnitChooser::~UnitChooser()
{
    delete ui;
}
