#include "neworder.h"
#include "ui_neworder.h"

#include <QCompleter>
#include <QRegExpValidator>

NewOrder::NewOrder(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::neworder)
{
    ui->setupUi(this);
    QPixmap pixmap(":paperclip.svg");
    ui->attachLbl->setPixmap(pixmap.scaled(15, 15));

    connect(ui->exitBtn, &QPushButton::clicked,
            this, &NewOrder::closeOrder);

    connect(ui->forwardBtn, &QPushButton::clicked,
            this, &NewOrder::forwardOrder);
}

NewOrder::~NewOrder()
{
    delete ui;
}

void NewOrder::closeOrder()
{

}

void NewOrder::forwardOrder()
{

}
