#include "userinfoscreen.h"
#include "ui_userinfoscreen.h"

UserInfoScreen::UserInfoScreen(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UserInfoScreen)
{
    ui->setupUi(this);
    this->setWindowTitle("Informacije o trenutnom korisniku");
}

UserInfoScreen::~UserInfoScreen()
{
    delete ui;
}
