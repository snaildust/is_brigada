#ifndef USERMAINPREVIEW_H
#define USERMAINPREVIEW_H

#include "unitchooser.h"
#include "warehousepreview.h"
#include "orderpreview.h"

#include <QDialog>

class UserInfoScreen;
class NewOrder;

namespace Ui {
class UserMainPreview;
}

class UserMainPreview : public QDialog
{
    Q_OBJECT

public:
    explicit UserMainPreview(QWidget *parent = 0);
    ~UserMainPreview();

public slots:
    void viewUserInfo();
    void chooseTroop();
    void viewOrders();
    void createNewOrder();
    void closeTab(int index);
    void showOrder();
    void showSentOrders();

private:
    Ui::UserMainPreview *ui;
    UserInfoScreen* userPreview;
    UnitChooser* unitChooser;
    WarehousePreview* warehousePreview;
    OrderPreview* orderPreview;
    NewOrder* newOrder;
    unsigned int unreadOrders; // TODO: add unread msgs etc
    unsigned int unreadClaims;
    unsigned int unreadAnnoun;
    unsigned int unreadPleas;
    unsigned int totalUnread;
    bool ordersLoaded;
    void loadReceivedOrders();
    void updateReadOrder(int orderno);
};

#endif // USERMAINPREVIEW_H
