#ifndef CARDVALIDATIONFORM_H
#define CARDVALIDATIONFORM_H

#include "usermainpreview.h"

#include <QDialog>

namespace Ui {
class CardValidationForm;
}

class CardValidationForm : public QDialog
{
    Q_OBJECT

public:
    explicit CardValidationForm(QWidget *parent = 0);
    ~CardValidationForm();

public slots:
    void authenticateAttempt();

private:
    int failedAttemptsCnt;
    UserMainPreview underlingsView;

    void authenticationAttemptFailed();
    void authenticationAttemptSucceded();

    void blockCurrentCard();

    Ui::CardValidationForm *ui;
};

#endif // CARDVALIDATIONFORM_H
