#-------------------------------------------------
#
# Project created by QtCreator 2017-12-10T16:41:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = brigada
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    userinfoscreen.cpp \
    cardvalidationform.cpp \
    usermainpreview.cpp \
    warehousepreview.cpp \
    unitchooser.cpp \
    orderpreview.cpp \
    neworder.cpp

HEADERS += \
        mainwindow.h \
    userinfoscreen.h \
    cardvalidationform.h \
    usermainpreview.h \
    warehousepreview.h \
    unitchooser.h \
    orderpreview.h \
    neworder.h

FORMS += \
        mainwindow.ui \
    userinfoscreen.ui \
    cardvalidationform.ui \
    usermainpreview.ui \
    warehousepreview.ui \
    unitchooser.ui \
    orderpreview.ui \
    neworder.ui

RESOURCES += \
    images.qrc \
    input_files.qrc

DISTFILES +=
