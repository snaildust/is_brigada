#ifndef USERINFOSCREEN_H
#define USERINFOSCREEN_H

#include <QDialog>

namespace Ui {
class UserInfoScreen;
}

class UserInfoScreen : public QDialog
{
    Q_OBJECT

public:
    explicit UserInfoScreen(QWidget *parent = 0);
    ~UserInfoScreen();

private:
    Ui::UserInfoScreen *ui;
};

#endif // USERINFOSCREEN_H
